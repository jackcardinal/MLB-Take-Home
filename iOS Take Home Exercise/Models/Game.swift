//
//  Game.swift
//  iOS Take Home Exercise
//
//  Created by Jack Cardinal on 2/1/23.
//

import SwiftUI


struct Game: Decodable, Identifiable {
    var id = UUID()
    let gamePk: Int
    let gameDate: Date
    let officialDate: String
    let teams: Teams
    let linescore: LineScore
    
    enum CodingKeys: CodingKey {
        case gamePk, gameDate, officialDate, teams, linescore
    }

}
struct Schedule: Decodable {
    let date: String
    var games: [Game]

    enum CodingKeys: CodingKey {
        case date, games
    }
}

struct ScheduleResponse: Decodable {
    let dates: [Schedule]
    
    enum CodingKeys: CodingKey {
        case dates
    }
}

struct LineScore: Decodable {
    let currentInning: Int
    let scheduledInnings: Int
    let innings: [Inning]
    
    enum CodingKeys: CodingKey {
        case currentInning, scheduledInnings, innings
    }
}

struct Teams: Decodable {
    let away: Away
    let home: Home
    
    enum CodingKeys: CodingKey {
        case away, home
    }
}

struct Away: Decodable {
    let score: Int
    let team: Team
    let isWinner: Bool
    
    enum CodingKeys: CodingKey {
        case score, team, isWinner
    }
    
}

struct Home: Decodable {
    let score: Int
    let team: Team
    let isWinner: Bool
    
    enum CodingKeys: CodingKey {
        case score, team, isWinner
    }
}

struct Team: Decodable {
    let teamName: String
    
    enum CodingKeys: CodingKey {
        case teamName
    }
}

struct Inning: Decodable {
    let num: Int
    
    enum CodingKeys: CodingKey {
        case num
    }
}


struct MockData {
        
    static let sampleSchedule = Schedule(date:"2023-03-06T18:05:00Z", games: [sampleGame1, sampleGame2])
    
    static let sampleGame1 = Game(gamePk: 123, gameDate: Date.now, officialDate: "2023-03-06T18:05:00Z", teams: sampleTeams, linescore: sampleLineScore)
    static let sampleGame2 = Game(gamePk: 123, gameDate: Date.now, officialDate: "2023-03-06T18:05:00Z", teams: sampleTeams, linescore: sampleLineScore)

    static let sampleLineScore = LineScore(currentInning: 3, scheduledInnings: 9, innings: [sampleInning1, sampleInning2])
    
    static let sampleTeams = Teams(away: sampleAway, home: sampleHome)
    
    static let sampleAway = Away(score: 5, team: sampleAwayTeam, isWinner: false)
    static let sampleHome = Home(score: 10, team: sampleHomeTeam, isWinner: false)
    
    static let sampleHomeTeam = Team(teamName: "Cardinals")
    static let sampleAwayTeam = Team(teamName: "NY Jets")

    static let sampleInning1 = Inning(num: 1)
    static let sampleInning2 = Inning(num: 2)
}


//- Each game in the list should display:
//  - On the left: the two teams playing, always starting with the 'away' team followed by the 'home' team
//  - In the middle: each team's name and score
//  - On the right: the state of the game:
//    - For games that have not yet started, show the starting time
//    - For games that are in progress, show the current inning
//    - For games that have already completed, show the word `Final`
//    - If a game was completed in more or fewer than the usual nine innings, show `F/#` where the # is the number of innings played (e.g. `F/6` or `F/10`)
//- Tapping a game in the list should display a detailed view of the game.  The choice of which data elements to display in the detail view, as well as the layout of those elements, are entirely up to you.

    
