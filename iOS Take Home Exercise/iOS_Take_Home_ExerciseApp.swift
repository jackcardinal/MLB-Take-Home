//
//  iOS_Take_Home_ExerciseApp.swift
//  iOS Take Home Exercise
//
//  Created by Jack Cardinal on 2/1/23.
//

import SwiftUI

@main
struct iOS_Take_Home_ExerciseApp: App {
    var body: some Scene {
        WindowGroup {
            MLBListView()
        }
    }
}
