//
//  LoadingView.swift
//  iOS Take Home Exercise
//
//  Created by Jack Cardinal on 2/1/23.
//

import SwiftUI

struct LoadingView: View {
    var body: some View {
        ZStack {
            Color(.systemBackground)
                .ignoresSafeArea(.all)
           // ActivityIndicator()
            //updated for iOS 15
            ProgressView()
                .progressViewStyle(CircularProgressViewStyle(tint: .gray))
                .scaleEffect(2)
                .offset(y: -150)
        }
    }
}

struct LoadingView_Previews: PreviewProvider {
    static var previews: some View {
        LoadingView()
    }
}
