//
//  MLBListViewModel.swift
//  iOS Take Home Exercise
//
//  Created by Jack Cardinal on 2/1/23.
//

import Foundation

final class MLBListViewModel: ObservableObject {
    
    @Published var selectedDate = Date() {
        didSet {
            print("viewModel.selectedDate \(selectedDate.description)")
            getGames(for: selectedDate)
        }
    }
    
//    var dates = [Schedule]() {
//        didSet {
//            games = dates.first!.games
//        }
//    }
    @Published var games: [Game] = []
    @Published var alertItem: AlertItem?
    @Published var isLoading = false
    @Published var isShowingDetailView = false
    
    private let dateFormatter = DateFormatter()
    
    var selectedGame: Game? {
        didSet {
            isShowingDetailView = true
        }
    }
    
    func getGames(for date: Date) {
        print("===============================>   getGames for selectedDate: \(date)")
        DispatchQueue.main.async { [self] in
            Task {
                isLoading = true
                do {
                    games = try await NetworkManager.shared.getGames(for: date)
                    isLoading = false
                } catch {
                    if let apError = error as? APError {
                        switch apError {
                        case .invalidURL:
                            alertItem = AlertContext.invalidURL
                        case .invalidResponse:
                            alertItem = AlertContext.invalidResponse
                        case .invalidData:
                            alertItem = AlertContext.invalidData
                        case .unableToComplete:
                            alertItem = AlertContext.unableToComplete
                        case .genericError:
                            alertItem = AlertContext.genericError
                        }
                    } else {
                        alertItem = AlertContext.genericError
                    }
                    isLoading = false
                }
            }
        }
        
    }
    
    func getRangeDate(string: String) -> Date {
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.date(from: string) ?? Date.now
    }
    

}



