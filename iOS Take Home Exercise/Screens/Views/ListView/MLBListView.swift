//
//  MLBListView.swift
//  iOS Take Home Exercise
//
//  Created by Jack Cardinal on 2/1/23.
//

import SwiftUI

struct MLBListView: View {
    
    @StateObject var viewModel = MLBListViewModel()
    
    // private let dateFormatter = DateFormatter()
    
    var body: some View {
        ZStack {
            NavigationStack {
                VStack(alignment: .center) {
                    ZStack {
                        VStack {
                            Image("MLB_logo_PNG7")
                                .resizable()
                                .frame(width: 35, height: 35, alignment: .center)
                            Divider()
                            DatePicker("", selection: $viewModel.selectedDate, displayedComponents: .date)
                                .datePickerStyle(.compact)
                                .labelsHidden()
                            Divider()
                        }
                    }
                    List(viewModel.games) { game in
                        MLBListViewCell(game: game)
                            .onTapGesture {
                                viewModel.selectedGame = game
                            }
                    }
                    
                    .listStyle(.inset)
                    
                }
                
                .blur(radius: viewModel.isShowingDetailView ? 20 : 0)
                
                .onAppear {
                    // initCalendar()
                    print( "listView onAppear")
                }
                
                .task {
                    viewModel.getGames(for: viewModel.selectedDate)
                }
                
                if viewModel.isLoading {
                    LoadingView()
                }
                
            }
            .navigationViewStyle(.stack)
        }
        .fullScreenCover(isPresented: $viewModel.isShowingDetailView, content: {
            MLBListDetailView(viewModel: viewModel)
        })
        .alert(item: $viewModel.alertItem) { alertItem in
            Alert(title: alertItem.title,
                  message: alertItem.message,
                  dismissButton: alertItem.dismissButton)
        }
    }
    
    private func initCalendar() {
        let calendar = Calendar(identifier: .gregorian)
        let components = DateComponents(year: 2018, month: 9, day: 18)
        if let customDate = calendar.date(from: components) {
            viewModel.selectedDate = customDate
        }
    }
}

struct MLBListView_Previews: PreviewProvider {
    static var previews: some View {
        MLBListView(viewModel: MLBListViewModel())
    }
}

struct MLBListViewCell: View {
    
    var viewModel = MLBListCellViewModel()
    
    let game: Game
    
    var body: some View {
        HStack(alignment: .center) {
            VStack(alignment: .leading) {
                Text(game.teams.away.team.teamName)
                Text(game.teams.home.team.teamName)
            }
            .frame(width: 100, alignment: .leading)
            .font(.title3)
            .padding(.leading, 3)
            Spacer()
            VStack(alignment: .center) {
                Text("\(game.teams.away.score)")
                Text("\(game.teams.home.score)")
            }
            .frame(width: 50, alignment: .center)
            .font(.title3)
            .fontWeight(.bold)
            Spacer()
            VStack(alignment: .trailing) {
                Text(viewModel.getDetails(for: game))
            }
            .frame(width: 100, alignment: .trailing)
            .font(.callout)
            .padding(.trailing, 3)
        }
    }
}


struct MLBListDetailView: View {
    
    @ObservedObject var viewModel: MLBListViewModel
    
    
    var body: some View {
        VStack {
            XDismissButton(isShowingDetailView: $viewModel.isShowingDetailView)
            VStack {
                Image("MLB_logo_PNG7")
                    .resizable()
                    .frame(width: 100, height: 100, alignment: .center)
                    .padding()
                Text(viewModel.selectedDate.formatted(date: .complete, time: .omitted))
                    .font(.title3)
                    .padding()
            }
            Divider()
            HStack(alignment: .center) {
                VStack(alignment: .leading) {
                    Text(viewModel.selectedGame!.teams.away.team.teamName)
                    Text(viewModel.selectedGame!.teams.home.team.teamName)
                    Text("Innings: ")
                    
                }
                .frame(width: 100, alignment: .leading)
                .font(.title2)
                .padding(.leading, 3)
                //Spacer()
                VStack(alignment: .center) {
                    Text("\(viewModel.selectedGame!.teams.away.score)")
                    Text("\(viewModel.selectedGame!.teams.home.score)")
                    Text("\(viewModel.selectedGame!.linescore.innings.count)")
                }
                .frame(width: 150, alignment: .center)
                .font(.title2)
                .fontWeight(.bold)
            }
            Spacer()
            
        }
        .padding()
    }
}
