//
//  MLBListCellViewModel.swift
//  iOS Take Home Exercise
//
//  Created by Jack Cardinal on 2/7/23.
//

import Foundation

final class MLBListCellViewModel: NSObject {
    
    func getDetails(for game: Game) -> String {
        let now = Date.now
        let gameDate = game.gameDate
        print("now: \(now)")
        //
        //print("gameDate: \(gameDate)")
        print("==== getDetails for this gqmeDate: \(gameDate)")

        let isInToday = Calendar.current.isDateInToday(gameDate)
        
        var output = "Default"
        
       // print("isInToday: \(isInToday)")
        
        if gameDate < now && !isInToday {
          //  print("past: \(gameDate)")
            if  game.linescore.innings.count < game.linescore.scheduledInnings {
                output =  "F/\(game.linescore.innings.count)"
            } else {
                output =  "Final"
            }
        } else if gameDate > now && !isInToday {
            //print("future: \(gameDate)")
            
            output = "\(gameDate.formatted(date: .abbreviated, time: .shortened))"

        } else if isInToday {
           // print("today: \(gameDate)")
            output = "\(gameDate.formatted(date: .abbreviated, time: .shortened))"
            if  game.linescore.innings.count < game.linescore.scheduledInnings {
                output =  "\(game.linescore.innings.count)"
            } else {
                output = "\(gameDate.formatted(date: .abbreviated, time: .shortened))"
            }
        }
          print("\(output)")

        return output
        
    }
    
}


//- For games that have not yet started, show the starting time
//- For games that are in progress, show the current inning
//- For games that have already completed, show the word `Final`
//- If a game was completed in more or fewer than the usual nine innings, show `F/#` where the # is the number of innings played (e.g. `F/6` or `F/10`)
//- Tapping a game in the list should display a detailed view of the game.  The choice of which data elements to display in the detail view, as well as the layout of those elements, are entirely up to you.
