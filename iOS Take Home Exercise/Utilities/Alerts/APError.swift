//
//  APError.swift
//  iOS Take Home Exercise
//
//  Created by Jack Cardinal on 2/1/23.

//

import Foundation

enum APError: Error {
    case invalidURL
    case invalidResponse
    case invalidData
    case unableToComplete
    case genericError
}
