
//
//  NetworkManager.swift
//  iOS Take Home Exercise
//
//  Created by Jack Cardinal on 2/1/23.

//

import SwiftUI


@MainActor final class NetworkManager {
    
    static let shared = NetworkManager()

    let baseURL = "https://statsapi.mlb.com/api/v1/schedule?hydrate=team(league),venue(location,timezone),linescore&date="
    let endPoint = "&sportId=1,51&language=en"
    
    private init () {}
    
    
    func getGames(for date: Date) async throws -> [Game] {
            
        let dateString = date.toDateString
        let urlString = baseURL + dateString + endPoint
        print("getGames network manager dateString \(dateString)")
        guard let url = URL(string: urlString ) else {
            throw APError.invalidURL
        }
        
        let (data, _ ) = try await URLSession.shared.data(from: url)
        
        do {
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .iso8601
            return try decoder.decode(ScheduleResponse.self, from: data).dates.first?.games ?? [Game]()
        } catch {
            throw APError.invalidData
        }
    }
}
