//
//  Date+Ext.swift
//  iOS Take Home Exercise
//
//  Created by Jack Cardinal on 2/1/23.
//

import Foundation

extension Date {
        
    var toDateString: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: self)
    }
//
}
